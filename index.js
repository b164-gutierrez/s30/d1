const express = require('express');

//mongoose is a package that allows creation of schemas to model our data structures.
// also has access to a number of methods for manipulation our database.
const mongoose = require('mongoose');

const app = express();

const port = 3001;

// mongoDB altas connection.
// when we want to use local mongoDB/robo3t.
// mongoose.connect('mondodb:localhost:27017/databaseName')
mongoose.connect('mongodb+srv://beear:Barryjosh08@cluster0.hnwmk.mongodb.net/batch164_to-do?retryWrites=true&w=majority', 
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	});

let db = mongoose.connection;

// connection error message.
db.on('error', console.error.bind(console, 'connection error'));

// connection successful message.
db.once('open', () => console.log('Were connected to the cloud database'))


app.use(express.json());

app.use(express.urlencoded({extended:true}));

// Mongoose Schemas
// Schema - determine the structure of the documents to be written in the database.
// act as 'BLUEPRINT' to ur data.
// use Schema() constructor of the Mongoose module to create a new Schema object.

// 'MODEL' sample
const taskSchema = new mongoose.Schema({
	name: String,
	status: {
		type: String,
		// default values are predefined values for a field if we dont put any value.
		default: 'pending'
	}
})

const Task = mongoose.model('Task', taskSchema);

// User Schema
const userSchema = new mongoose.Schema({
	username: String,
	password: String
})

const User = mongoose.model('User', userSchema);

app.post('/signup', (req, res) => {
	User.findOne({username: req.body.username}, (err, result) =>{
		if(result != null && result.username == req.body.username){
			return res.send('Already registered');
		} else {
			let newUser = new User({
				username: req.body.username
			});

			newUser.save((saveErr, savedUser) => {
				if(saveErr){
					return console.log(saveErr)
				} else {
					return res.status(201).send('New user registered')
				}
			})
		}
	})
})


// get /users
app.get('/users', (req, res) => {
	User.find({}, (err, result) => {
		if(err){
			return console.log(err);
		} else {
			return res.status(200).json({
				usersdata: result
			})
		}
	})
})


// Routes.Endpoints
// Creating a new task
// Business Logic

/*
1. Add a functionality to check if there are duplicates tasks
	-If the task already exists in the database, we return error.
	-If the task doesnt exists in the database, we add it in the database.
		1. Task data will be coming from request's body.
		2. Create new Task object with field/property.
		3. Save the new object to database.
*/

// sample
app.post('/tasks', (req, res) => {
	Task.findOne({name: req.body.name}, (err, result) => {

		// if a document was found and the document's name matches the information sent via the client.
		if(result != null && result.name == req.body.name){
			return res.send('Duplicate task found');
		} else{
			//if no document was found.
			 //create a new task and save it to the database.
			 let newTask = new Task({
			 	name: req.body.name
			 });

			 newTask.save((saveErr, savedTask) => {
			 	// if there are errors in saving
			 	if(saveErr){
			 		return console.error(saveErr)
			 	} else{
			 		return res.status(201).send('New task created')
			 	}
			 })
		}
	})
})


// get all tasks
// Business Logic

/*
	1. Find/retrieve all the documents.
	2. If an error is encountered, print the error.
	3. If no errors are found, send a success status back to the client and return an array of documents.
*/
app.get('/tasks', (req, res) => {
	Task.find({}, (err, result) => {

		if(err){
			return console.log(err);
		} else {
			return res.status(200).json({
				dataFromMDB: result
			})
		}
	})
})






/*

Activity:
1. Create a User schema.
2. Create a User model.
3. Create a POST route that will access the "/signup" route that will create a user.
4. Process a POST request at the "/signup" route using postman to register a user.
5. Create a GET route that will return all users.
6. Process a GET request at the "/users" route using postman.
7. Create a git repository named S30.
8. Initialize a local git repository, add the remote link and push to git with the commit message of s30 Activity.
9. Add the link in Boodle named Express js Data Persistence via Mongoose ODM.


Business Logic in Creating a user '/signup'
	1. Add a functionality to check if there are duplicates tasks.

		a. If the user already exists, return error or 'Already registered'.	
		b. If the user does not exists, we add it on our 	database.

			i. The user data will be coming from the req.body.
			ii. Create a new User object with a 'username' and 'password' fields/properties.
			iii. Then save, and add an error handling.
*/






















app.listen(port, () => console.log(`Server running at port ${port}`) );



